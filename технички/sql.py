#_____________________________________________#
#____________________ ОБЩАЯ ТЕОРИЯ ПО БД ______
#
# Типы связей:
# - 1 к 1
# - 1 ко многим
# - Многие ко многим
#
#  Нормализация - это удаление избыточности данных. Иначе, избыточность ведет к увеличению места на диске,
# аномалиям включения, обновления, удаления.
#  Устранение избыточности, как правило, производится за счет декомпозиции отношений таким образом, чтобы
# в каждом отношении хранились только первичные факты (то есть факты, не выводимые из других хранимых фактов)
# Нормальные формы:
# 1 НФ. В каждой клетке таблицы должно быть только одно значение, не должно быть повторяющихся строк
# 2 НФ. Таблица в 1НФ. Есть первичный ключ. Все атрибуты зависят от первичного ключа целиком,
#       а не от какой-то его части
# 3 НФ. Таблица во 2НФ. Все атрибуты зависят только от первичного ключа, но не от других атрибутов
#
# Процесс ограничения данных называется обеспечением целостности данных.
# Целостность БД - это свойство БД, которое обеспечивает корректность и
# непротиворечивость данных в любой момент времени.
# Существует три вида целостности баз данных:
# - Доменная целостность: CHECK (ограничение проверки), DEFAULT (запись значения по-умолчанию)
# - Целостность сущностей: Primary Key (первичный ключ), UNIQUE (уникальность)
# - Ссылочная целостность: Foreign Key (внешний ключ)
#
# Ключ - это колонка или колонки, не имеющие в строоках дублирующих значений.
# Кроме того, колонки должны быть неприводимо уникальными (2НФ), то есть никакое подмножество
# колонок не обладает такой уникальностью.
#
# Порядок выполнения SQL запроса:
# 1) FROM
# 2) ON
# 3) JOIN
# 4) WHERE
# 5) GROUP BY
# 6) HAVING
# 7) SELECT
# 8) DISTINCT
# 9) ORDER BY
#
#
#_____________________________________________#
#____________________ SELECT __________________
#
# >>> SELECT DISTINCT(name) as column_name FROM table_name LIMIT 10
# *** ПОЛУЧИТЬ УНИКАЛЬНЫЕ ЗНАЧЕНИЯ(name) как column_name ИЗ ТАБЛИЦЫ table_name ОГРАНИЧИТЬ первые 10
#
# В некоторых случаях можно выполнять SELECT без FROM. Например:
# >>> SELECT NOW()
# *** Получаем текущую дату
#
#_____________________________________________#
#____________________ WHERE __________________
#
# >>> SELECT * FROM sh WHERE year BETWEEN 2000 AND 2005 AND eye LIKE 'Blue%'
# *** ПОЛУЧИТЬ все ИЗ sh ГДЕ year МЕЖДУ 2000 и 2005 и eye КАК 'Blue%'
#
# Доступные операторы:
# = Равно, <> != Неравно, > Больше, < Меньше, <= Меньше или равно,
# between Значение находится в указанном диапозоне,
# in ('Значение1', 'Значение2', ...) Значение входит в список,
# like '%Blond%' Проверка строки на соответствие шаблону (% - любое кол-во символов, включая 0; _ - ровно один символ)
#
# Логические операции:
# AND - И, OR - ИЛИ, NOT - НЕ
#
#
#_____________________________________________#
#____________________ ORDER BY ________________
#
# >>> SELECT * FROM sh ORDER BY year ASC, appearances
# *** ПОЛУЧИТЬ все ИЗ sh ОТСОРТИРОВАТЬ ПО year ПО ВОЗРАСТАНИЮ, appearances (в каждом году будут отсорт. по кол-во появлений)
#
# ASC - по возрастанию, DESC - по убыванию
#
#
#_____________________________________________#
#____________________ CREATE TABlE ____________
#
# >>> CREATE TABLE sh (
#       id SERIAL PRIMARY KEY,  # (для postgresql, в других субд по другому)
#       name VARCHAR(100)
#   )
# *** СОЗДАТЬ ТАБЛИЦУ sh (
#   id ЦЕЛОЕ ЧИСЛО УНИКАЛЬНЫЙ ПЕРВИЧНЫЙ КЛЮЧ АВТОМАТИЧЕСКИЙ,  (для postgresql)
#   name Строка переменной длины(100)
#   )
#
# Типы данных в SQL:
#___________________________________________________________________________________________________________________
# CHARACTER(n) (CHAR(n))             | Строка фиксированной длины n
# CHARACTER VARYING(n) (VARCHAR(n))  | Строка переменной длины, максимальная длина n
# BOOLEAN                            | Логический тип данных
# INTEGER (INT)                      | Целое число
# NUMERIC(p,s)                       | Действительное число (p - кол-во значащих цифр, s - кол-во цифр после запятой)
# REAL                               | Действительное число одинарной точности
# DOUBLE PRECiSION                   | Действительное число двойной точности
# DATA                               | Дата
# TIMESTAMP                          | Дата и время
#___________________________________________________________________________________________________________________
#
#
#_____________________________________________#
#____________________ DROP TABlE ______________
#
# >>> DROP TABLE IF EXISTS sh;
#     CREATE TABLE sh ( id INT );
# *** УДАЛИТЬ ТАБЛИЦУ ЕСЛИ СУЩЕСТВУЕТ sh;
#     СОЗДАТЬ ТАБЛИЦУ sh ( id ЧИСЛО );
#
#
#_____________________________________________#
#____________________ ALTER TABLE _____________
# более подробно об этом в Postgres https://postgrespro.ru/docs/postgresql/9.6/sql-altertable
#
# >>> ALTER TABLE sh ADD COLUMN alive BOOLEAN;
#     ALTER TABLE sh ALTER COLUMN alive INT NOT NULL;
#     ALTER TABLE sh DROP COLUMN year;
#     ALTER TABLE sh RENAME COLUMN name TO hero_name;
#     ALTER TABLE sh RENAME TO superheroes;
# *** ИЗМЕНИТЬ ТАБЛИЦУ sh ДОБАВИТЬ КОЛОНКУ alive ЛОГИЧЕСКИЙ ТИП;
#     ИЗМЕНИТЬ ТАБЛИЦУ sh УДАЛИТЬ КОЛОНКУ year;
#     ИЗМЕНИТЬ ТАБЛИЦУ sh ПЕРЕИМЕНОВАТЬ КОЛОНКУ name В hero_name
#     ИЗМЕНИТЬ ТАБЛИЦУ sh ПЕРЕИМЕНОВАТЬ В superheroes;
#
#
#_____________________________________________#
#____________________ INSERT INTO _____________
#
# >>> INSERT INTO superheroes(name, appearances, universe) VALUES ('Spider-Man', 4043, 'marvel')
# *** ВСТАВИТЬ В superheroes (колонка1, колонка2, колонка3) ЗНАЧЕНИЯ ('значение 1', значение2, 'значение 3')
#
# Можно так же выполнять с подзапросом
# >>> INSERT INTO GoodTypes VALUES((SELECT MAX(good_type_id)+1 FROM GoodTypes), 'auto')
#   --- Это не сработает в MySQL. Нужно добавить AS в подзапрос (SELECT MAX(good_type_id)+1 FROM GoodTypes AS a)
#
#_____________________________________________#
#____________________ UPDATE __________________
#
# >>> UPDATE superheroes SET name='BATMAN', universe='dc' WHERE gender='Male Characters'
# *** ИЗМЕНИТЬ ДАННЫЕ В superheroes УСТАНОВИТЬ name = 'BATMAN', universe = 'dc' ГДЕ gender='Male Characters' (изменит все строки с этим значением)
#
#
#_____________________________________________#
#____________________ DELETE FROM _____________
#
# >>> DELETE FROM superheroes WHERE gender='Male Characters'
# *** УДАЛИТЬ (СТРОКИ) ИЗ superheroes ГДЕ gender='Male Characters'
#
# >>> DELETE FROM superheroes
# *** УДАЛИТЬ ВСЕ ДАННЫЕ ИЗ superheroes
#
#
#_____________________________________________#
#____________________ AVG, COUNT, MAX, MIN, SUM
#____________________ GROUP BY ________________
#
# Агрегатные функции - функции, которые используется вместе с группировкой
# Они получают несколько значений и для них вычисляют одну значение
#
# AVG   | Среднее значение
# COUNT | Кол-во значений
# MAX   | Максимальное значение
# MIN   | Минимальное значение
# SUM   | Сумма
#
# >>> SELECT align, AVG(appearances), SUM(appearances)/COUNT(*) AS average FROM superheroes GROUP BY align
# *** ПОЛУЧИТЬ align, СРЕДНЕЕ(appearances), СУММА(appearances)/КОЛ-ВО(всех) КАК average ИЗ superheroes ГРУППИРОВАТЬ ПО align
#   --- AVG рассчитает значение точно, а при получении average будет целое число (в postgres при делении двух целых чисел получаем целое число)
#
# >>> SELECT COUNT(*), SUM(appearances), MAX(appearances) FROM superheroes
# *** ПОЛУЧИТЬ КОЛ-ВО(всех), СУММА(appearances), МАКСИМУМ(appearances) ИЗ superheroes
#   --- Здесь используем агрегатные функции без группировки (GROUP BY), т.к. вся таблица будет считаться одной большой группой
#
#
#_____________________________________________#
#____________________ HAVING __________________
#
# >>> SELECT hair, COUNT(*) FROM superheroes WHERE gender='Female' GROUP BY hair HAVING COUNT(*) BETWEEN 50 and 300
# *** ПОЛУЧИТЬ hair, КОЛ-ВО(всех) ИЗ superheroes ГДЕ gender='Female' ГРУППИРОВАТЬ ПО hair СОДЕРЖАШИЙ COUNT(*) > 10 СОДЕРЖАЩИЕ COUNT(*) МЕЖДУ 50 И 300
#   --- Условия, которые указываются в HAVING запускаются после того, как будут созданы группы. WHERE запускается до создания групп
#       WHERE так же можно использовать совместно с GROUP BY, однако, если идет поиск по результатам агрегатных функций, то нужно использовать HAVING
#
#
#_____________________________________________#
#____________________ JOIN ____________________
#
# >>> SELECT products.name, product_types.type_name FROM products JOIN product_types ON products.type_id = product_types.id
# *** ПОЛУЧИТЬ products.name, product_types.type_name ИЗ products ОБЪЕДИНИТЬ product_types НА products.type_id = product_types.id
#   --- Данный тип объединения называется ВНУТРЕННИМ. В него входят те строки, для которых нашлись соответств. строки из другой таблицы
#
#   По умолчанию, используется INNER JOIN - внутреннее объединение
#   Другие объединения:
#   - LEFT OUTER JOIN - левое внешнее объединение
#   В него входят строки из таблицы, которая находится слева от JOIN (в примере: 'products')
#   - RIGHT OUTER JOIN - правое внешнее объединение
#   В него входят строки из таблицы, которая находится справа от JOIN (в примере: 'product_types')
#   - FULL OUTER JOIN - полное внешнее объединение
#   В него входят строки из всех таблиц
#   - CROSS JOIN - перекрестное объединение
#   Строится при комбинации строк двух таблиц "Каждая с каждой"
#   Условие объединения не указывается! Например >>> SELECT products.name, product_types.type_name FROM products CROSS JOIN product_types
#
# Объединение нескольких (> 2) таблиц
# SELECT p.id, p.name, p.price FROM products AS p JOIN sales AS s ON p.id = s.product_id JOIN orders AS o ON o.id = s.order_id
#
# Можно объединять таблицу с самой собой
# >>> SELECT e.name FROM employee AS e JOIN employee AS m ON e.managerid = m.id
#
#
#_____________________________________________#
#____________________ UNION ___________________
#
# При объединении нужно соблюсти условия:
# 1) Число и порядок извлекаемых столбцов должны совпадать во всех объединяемых запросах
# 2) Типы данных в соответствующих столбцах должны быть совместимы
# Объединять можно любое кол-во таблиц
# >>> SELECT * FROM test1
#     UNION ALL
#     SELECT * FROM test2
# При использовании конструкции UNION ALL в результат попадут все строки (в том числе и дубли)
# Если использовать только UNION, то в дубликаты строк будут удалены при объединении
#
#
#_____________________________________________#
#____________________ MINUS/EXCEPT ____________
#
# Так же, как UNION используется для работы с множествами
# В рузельтате его выполнения остаются только те строки, которые были в test1, но не было в test2
# >>> SELECT * FROM test1
#     MINUS
#     SELECT * FROM test2
# В Postgres этот оператор называется EXCEPT
#
#
#_____________________________________________#
#____________________ INTERSECT _______________
#
# Противоположен MINUS. При использовании INTERSECT получаем совпадение строк между test1 и test2
# >>> SELECT * FROM test1
#     INTERSECT
#     SELECT * FROM test2
#
#
#_____________________________________________#
#____________________ ПОДЗАПРОСЫ ______________
#
# >>> SELECT id, name, price FROM products WHERE id IN (SELECT product_id FROM sales)
# *** ПОЛУЧИТЬ id, name, price ИЗ products ГДЕ id В (ПОЛУЧИТЬ product_id ИЗ sales)
#   --- Подзапрос заключен в скобках, выполняется первым
#
# Можно использовать не только в SELECT. Пример для UPDATE:
# >>> UPDATE products SET price = price + 500 WHERE type_id = (SELECT id FROM product_types WHERE type_name='Книга')
#
# Также, можно использовать подзапрос в подзапросе:
# >>> DELETE FROM person WHERE id NOT IN (SELECT * FROM (SELECT MIN(id) FROM person GROUP BY email) AS de)
#
#
#_____________________________________________#
#____________________ ТРАНЗАКЦИИ ______________
#
# >>> START TRANSACTION;
#     UPDATE accounts SET balance = balance - 15000 WHERE account_number = 1234567;
#     UPDATE accounts SET balance = balance + 15000 WHERE account_number = 9876543;
#     COMMIT; (иначе ROLLBACK;)
# *** Начинаем транзакцию;
#     Какие-то действия...;
#     Записываем изменени
#     ФИКСИРУЕМ ТРАНЗАКЦИЮ; (иначе ОТМЕНЯЕМ;)
#
#
#_____________________________________________#
#____________________ ОГРАНИЧЕНИЯ _____________
#
# PRIMARY KEY (первичный ключ) - Обеспечивает уникальность данных (создает ограничение уникальности для столбца,
#                                на котором задается), предоставляет ссылку для связи с другими таблицами.
#                                Может быть только один.
# FOREIGN KEY - внешний ключ (колонка в таблице ИЗ которой ссылаемся)
# NOT NULL - не пустое значение
# UNIQUE - столбец должен содержать уникальное значение (можно использовать вместе UNIQUE NOT NULL)
#          Является потенциальным (альтернативным) ключом
# CHECK - проверка. Например: CHECK (price >= 0)
#
# Ограничениям можно присваивать имя с помощью CONSTRAINT. Например:
# >>> CREATE TABLE products(
#        id PRIMARY KEY,       # Ограничение NOT NULL накладывается по умолчанию
#        price INT CONSTRAINT positive_price CHECK (price >= 0)
#       )
# Если имя не указано, то субд присвоит имя по умолчанию
#
# Ограничения ссылочное целостности (внешнего ключа), мы должны указать куда ссылается наш столбец
# ..., type_id INT REFERENCES product_types(id), ...
# Теперь субд знает, что столбец type_id ссылается на product_types.id
# и если мы попытаемся запись в product_types, то субд предпримет действия, чтобы предотвратить несогласованность данных.
# Этим ограничением можно управлять:
#   type_id INT REFERENCES product_types(id) \
#       ON DELETE RESTRICT - в этом случае субд запретит удаление из таблицы product_types данных, на которые есть ссылки
#       ON DELETE CASCADE - здесь, если сделаем удаление из product_types, все записи из таблицы products ссылающиеся на него будут удалены каскадно
#       ON DELETE SET NULL - записываем null в колонку таблицы с внешним ключом
#       ON DELETE SET DEFAULT - устанавливаем дефолтное значение
#       ON UPDATE RESTRICT - субд будет запрещать нам изменять строки, на которые есть ссылки
#       ON UPDATE CASCADE - данные будут изменены в таблице из которой ссылаемся
#
#
#_____________________________________________#
#____________________ ПРЕДСТАВЛЕНИЯ ___________
#
# >>> CREATE VIEW customers_v id, name AS SELECT id, name FROM customers
# *** СОЗДАТЬ ПРЕДСТАВЛЕНИЕ название столбец1, столбец2 ДАЛЕЕ УКАЗЫВАЕМ ЗАПРОС
# Запрос к этому представлению: SELECT * FROM customers_v
#
# Представления с данными из нескольких таблиц:
# >>> CREATE VIEW products_v
#     AS SELECT p.id AS id,
#               p.name AS product_name,
#               t.type_name AS product_type,
#               p.price AS product_price
#     FROM products AS p JOIN product_types AS t ON p.type_id = t.id
#
# Представления не содерджат данных. Когда мы обращаемся к представлению, данные берутся из таблиц.
# Но есть другой вариант - MATERIALIZED VIEW, которые данные содержат (есть не во всех субд)
# >>> CREATE MATERIALIZED VIEW products_mv AS SELECT id FROM products
# Чтобы обновить данные в таком представлении:
# >>> REFRESH MATERIALIZED VIEW products_mv
#
# >>> DROP VIEW products_v
# *** УДАЛИТЬ ПРЕДСТАВЛЕНИЕ products_v
#
# >>> DROP MATERIALIZED VIEW products_mv
# *** УДАЛИТЬ МАТЕРИАЛИЗОВАННОЕ ПРЕДСТАВЛЕНИЕ products_mv
#
#
#_____________________________________________#
#____________________ WITH ____________________
#
# >>> WITH s1 AS (
#       SELECT classroom, COUNT(classroom) AS c FROM Schedule
#       GROUP BY classroom ORDER BY c DESC),
#          s2 AS (SELECT c FROM s1 LIMIT 1)
#     SELECT classroom FROM s1 WHERE c = (SELECT c FROM s2)
# *** ПОДГОТАВЛИВАЕМ ЗАПРОС s1 (...), s2 (...)
#     ПОЛУЧИТЬ classroom ИЗ s1 ГДЕ c = (ПОЛУЧИТЬ c ИЗ s2)
#   --- ИНСТРУКЦИЯ WITH позволяет подготовить запрос(ы), а затем использовать его(их) в другом запросе
#
#
#_____________________________________________#
#____________________ WITH RECURSIVE __________
#
# >>> WITH RECURSIVE temp1 AS (
# SELECT t1.id, t1.parent_id, t1.name, CAST (t1.name AS VARCHAR (50)) AS path
# 	FROM he t1 WHERE t1.name = 'Москва'
# UNION
# SELECT t2.id, t2.parent_id, t2.name, CAST (temp1.path || '->' || t2.name AS VARCHAR(50))
# FROM he t2 JOIN temp1 ON (temp1.parent_id = t2.id))
# SELECT * FROM temp1
# *** Сначала выполняется первый запрос. Потом к его результатам добавляются результаты второго запроса,
# где данные таблица temp1 – это результат первого запроса. Затем снова выполняется второй запрос,
# но данные таблицы temp1 – это уже результат предыдущего выполнения второго запроса. И так далее.
# После этого данные этой таблицы можно использовать в основном запросе как обычно.
#
#
#_____________________________________________#
#____________________ ФУНКЦИИ _________________
#
# >>> SELECT LENGTH(поле) FROM имя_таблицы WHERE условие
# Функция LENGTH используется для подсчета количества символов в строках.
# Вместо length можно использовать следующие названия: octet_length, char_length, character_length.
# Существует также функция bit_length, которая возвращает длину в битах.
#
# >>> SELECT 'Средняя цена = '+ CAST(AVG(price) AS CHAR(15)) FROM Laptop;
# CAST используется для преобразования к какому-то типу данных.
# В данном примере использована для конкатенации числа со строкой
#
#
#_____________________________________________#
#____________________ ВСПОМОГАТЕЛЬНЫЕ ОПЕРАТОРЫ
#
# || - конкатенация строк
# string1 || string2 || string_n
# >>> SELECT 'Однажды ' || 'в студеную' || ' зимнюю ' || 'пору'
#
#
#
#
#