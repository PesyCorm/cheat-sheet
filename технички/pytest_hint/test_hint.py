import pytest
#_____________________________________________#
#____________________ ФИКСТУРЫ ________________
#
# Для создания расширенной фикстуры (обычными являются setup, teardown и тд) в PyTest необходимо:
#
# 1) импортировать модуль pytest
# 2) использовать декоратор @pytest.fixture(), чтобы обозначить что данная функция является фикстурой
# 3) задать уровень фикстуры (scope). Возможные значения “function”, “cls”, “module”, “session” (только в conftest.py). Значение по умолчанию = “function”.
# 4) если необходим вызов teardown для этой фикстуры, то надо добавить в него финализатор (через метод addfinalizer объекта request передаваемого в фикстуру)
# 5) добавить имя данной фикстуры в список параметров функции

@pytest.fixture()  # Если в качестве аргумента укажем autouse=True, то фикстура будет использована для всех тестов
def resource_setup(request):
    print("resource_setup")

    def resource_teardown():
        print("resource_teardown")

    request.addfinalizer(resource_teardown)
    # Как возможность, фикстура в PyTest может возвращать что-нибудь в тест через return
    return {"Red":1,"Blue":2,"Green":3}

# Альтернативный вариант teardown фикстуры - через использование конструкции yield
# Например:
@pytest.fixture()
def resource_setup2():
    print("resource_setup2")
    yield
    print("resource_teardown2")

def test_1_that_needs_resource(resource_setup):
    print("test_1_that_needs_resource")
    assert resource_setup['Red'] == 1  # Здесь мы используем возвращаемый фикстурой объект (стр. 22)

def test_2_that_does_not():
    print("test_2_that_does_not")

# Так же, можно вызвать фикстуру с помощью декоратора
@pytest.mark.usefixtures("resource_setup2")
def test_3_that_does_again():
    print("test_3_that_does_again")
    # assert resource_setup2['Red'] == 1     - Похоже, что в данном варианте это работать не будет

# Фикстуры можно описывать в файле conftest.py, который автоматически импортируется PyTest.
# При этом фикстура может иметь любой уровень (только через описание в этом файле можно создать фикстуру с уровнем «сессия»).
#
# В примере создания расширенной фикстуры (стр. 13-...) мы передали в нее параметр request.
# Это было сделано чтобы через его метод addfinalizer добавить финализатор.
# Однако этот объект имеет также достаточно много атрибутов и других методов
# !Полный список в официальном API http://pytest.org/latest/builtin.html#fixtures-and-requests.
@pytest.fixture(scope="function")
def request_methods(request):
    print(request.fixturename)
    print(request.scope)
    print(request.function.__name__)
    print(request.cls)
    print(request.module.__name__)
    print(request.fspath)

def test_1_check_request_methods(request_methods):
    pass
#
# PyTest не ограничивает список фикстур вызываемый для теста.
#
@pytest.fixture()
def multi_fixture1():
    print('multi fixture1 called')

# Любая фикстура может вызывать любое кол-во фикстур внутри себя
@pytest.fixture()
def multi_fixture2():
    print('multi fixture2 called')

@pytest.fixture()
def multi_fixture3(multi_fixture2):
    print('multi fixture3 called')

def test_multi_fixtures(multi_fixture1, multi_fixture3):
    print('test multi fixtures')
#
#
#_____________________________________________#
#____________________ ПАРАМЕТРИЗАЦИЯ __________
#
# Параметризация — это способ запустить один и тот же тест с разным набором входных параметров.
#
# Задать параметры для теста можно двумя способами:
# 1) Через значение параметра params фикстуры, в который нужно передать массив значений.
# То есть фактически фикстура в данном случае представляет собой обертку, передающую параметры.
# А в сам тест они передаются через атрибут param объекта request, описанного выше.
# 2) Через декоратор (метку) @pytest.mark.parametrize, в который передается список названий переменных и массив их значений.
#
# --- ПЕРВЫЙ СПОСОБ ---
#
def strange_string_func(str):
    if len(str) > 5:
        return str + "?"
    else:
        return str + "!"

@pytest.fixture(scope="function", params=[
    ("abcdefg", "abcdefg?"),
    ("abc", "abc!")],
    ids=["len>5","len<=5"]) # Параметр ids принимает список имен тестов (его длина должна совпадать с количеством оных)
                            # ids необязательный, но без него трудно понять, что за параметр был передан в тест
def param_test(request):
    return request.param


def idfn(val):
    return "params: {0}".format(str(val))

@pytest.fixture(scope="function", params=[
    ("abcdefg", "abcdefg?"),
    ("abc", "abc!")],
    ids=idfn) # Вместо списка имен, можно передать функцию, которая сгенерирует итоговое название.
def param_test_idfn(request):
    return request.param

def test_strange_string_func(param_test):
    (input, expected_output) = param_test
    result = strange_string_func(input)
    assert result == expected_output
#
# --- ВТОРОЙ СПОСОБ ---
#
# Второй способ имеет одно преимущество: если указать несколько меток с разными параметрами,
# то в итоге тест будет запущен со всеми возможными наборами параметров
@pytest.mark.parametrize("x", [-1,2], ids=["negative x","positive x"])
@pytest.mark.parametrize("y", [-10,11], ids=["negative y","positive y"])
def test_cross_params_2(x, y):
    print("x: {0}, y: {1}".format(x, y))
#
#
#_____________________________________________#
#____________________ МЕТКИ (marks) ___________
#
# PyTest поддерживает класс декораторов @pytest.mark называемый «метками» (marks). Базовый список включает в себя следующие метки:
#
# 1) @pytest.mark.parametrize — для параметризации тестов (было рассмотрено выше)
# 2) @pytest.mark.xfail – помечает, что тест должен не проходить и PyTest будет воспринимать это, как ожидаемое поведение.
# Также эта метка может принимать условие, при котором тест будет помечаться данной меткой.
import sys

@pytest.mark.xfail(sys.platform != "win64", reason="requires windows 64bit")
def test_failed_for_not_win32_systems():
    assert False
# 3) @pytest.mark.skipif – позволяет задать условие при выполнении которогл тест будет пропущен
@pytest.mark.skipif(sys.platform != "win64", reason="requires windows 64bit")
def test_skipped_for_not_win64_systems():
    assert False
# 4) @pytest.mark.usefixtures – позволяет перечислить все фикстуры, вызываемые для теста (было рассмотрено выше)
# 5) Произвольные пользовательские метки. Они позволяют выделять наборы тестов для отдельного запуска по имени метки
# Для запуска тестов с определенным метками используем: pytest -m "critical_tests"
@pytest.mark.critical_tests
def test_2():
    print("test_2")
# Также меткой можно пометить не только тест, но и класс
@pytest.mark.level2
class TestClass:
    pass
# Можно пометить модуль с помощью глобально переменной pytestmark
pytestmark = pytest.mark.level1 # Для нескольких маркеров используем список: [pytest.mark.webtest, pytest.mark.slowtest]
#
# На пользовательские метки pytest будет кидать warning, т.к. не знает о них.
# Чтобы сообщить ему о метках, нужно зарегестрировать их в pytest.ini (см. пример в файле pytest.ini)
# Вообще список шире и его можно получить выполнив команду «pytest --markers».
#
#
#_____________________________________________#
#____________________ ОБРАБОТКА ИСКЛЮЧЕНИЙ ____
def f():
    print(1/0)

def test_exception():
    with pytest.raises(ZeroDivisionError):
        f()
#
#
#
#
#
#
#
# ДОБАВИТЬ ПРО ХУКИ!!!!!!!!!!