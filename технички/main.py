#__________________________________________________________________#
#______________________Реализация итератора_________________________
#
# Итератор - это объект, который поддерживает функцию next()
# для перехода к следующему элементу коллекции.
# По его элементам можно пройтись только один раз.
# Итерируемый объект - это объект, который позволяет поочередно
# обойти свои элементы и может быть преобразован к итератору
#

class StringByLetter:
    def __init__(self, string):
        self.string = string
        self.str_len = len(string)
        self.position = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.position < self.str_len:
            letter = self.string[self.position]
            self.position += 1
            return letter.upper()
        raise StopIteration


for letter in StringByLetter('hello world'):
    print(letter)


#________________________ Реализация генератора_____________________

def string_by_letter(string):
    for letter in string:
        yield letter.upper()

for letter in string_by_letter('hello world'):
    print(letter)

# Генератор - это более удобный способ реализовать протокол итератора
# Генератор является инстансом итератора

from collections.abc import Iterator
print(isinstance(string_by_letter('hello world'), Iterator))

#__________________________________________________________________#
#____________________Простая реализация декоратора__________________

current_user = {'role': 'admin'}

def check_access(func):
    def wrapper(*args, **kwargs):
        # Если хотим принимать аргументы в функцию с декоратором - то аргументы нужно также указывать в декораторе,
        # т.к. он будет вызван первым
        if current_user.get('role') != 'admin':
            raise Exception('Доступ запрещен')
        return func(*args, **kwargs)
    return wrapper

@check_access
def do_admin_work(*args, **kwargs):
    pass

print('Название функции do_admin_work изменяется на название функции из декоратора: ', do_admin_work.__name__)

#__________________________________________________________________#
#__________________________Python___________________________________
#
#>>> В качестве ключа для словарей можно использовать объекты, которые
#    имеют методы __eq__ и __hash__ т.к. в словаре поиск происходит по
#    хешу объекта
#
#>>> Сборщик мусора проверяет объекты, которые не были удалены могут переместиться
# в категорию средне/долго живущие. В таком случае, их он будет проверять реже.
#
#>>> В MRO3 используется поиск в ширину, можно посмотреть через ClassName.mro()
#